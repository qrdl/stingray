/**************************************************************************
 *
 *  File:       sr_internal.h
 *
 *  Project:    Stingray (https://gitlab.com/qrdl/stingray)
 *
 *  Descr:      Managed strings library
 *
 *  Comments:   Internal declarations, this functions, type and macros
 *              should not be used directly by application code
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef STINGRAY_INTERNAL_H
#define STINGRAY_INTERNAL_H

#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>

struct sr {
    char	    *val;	// c-string
    size_t	    size;	// allocated size
    size_t	    len;	// actual length of val (not incl. \0)
};

/* if compiler isn't GCC, ##__VA_ARGS__ cannot be used, therefore NUMARGS
 * with no arguments will produce error */
#ifndef NUMARGS
#define NUMARGS(...) NUMARGS__(0, ##__VA_ARGS__, 15, 14, 13, 12, 11, 10, 9, 8, \
       7, 6, 5, 4, 3, 2, 1, 0)
#define NUMARGS__(Z, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, \
        _14, _15, N, ...) N
#endif

#define SR_DATATYPE_INT     1
#define SR_DATATYPE_UINT    2
#define SR_DATATYPE_SHRT    3
#define SR_DATATYPE_USHRT   4
#define SR_DATATYPE_LNG     5
#define SR_DATATYPE_ULNG    6
#define SR_DATATYPE_LLNG    7
#define SR_DATATYPE_ULLNG   8
#define SR_DATATYPE_CHR     9
#define SR_DATATYPE_UCHR    10
#define SR_DATATYPE_STR     11
#define SR_DATATYPE_USTR    12
#define SR_DATATYPE_FLT     13
#define SR_DATATYPE_DBL     14
#define SR_DATATYPE_SR      15

static inline char *sr_string_val(int dummy, ...) {
    va_list ap;
    va_start(ap, dummy);
    struct sr *tmp =(struct sr *)va_arg(ap, struct sr *);
    va_end(ap);
    return tmp->val;
}

/* generic param processing for variables */
#define S(A) _Generic((A), \
        int:                    SR_DATATYPE_INT, \
        unsigned int:           SR_DATATYPE_UINT, \
        short:                  SR_DATATYPE_SHRT, \
        unsigned short:         SR_DATATYPE_USHRT, \
        long:                   SR_DATATYPE_LNG, \
        unsigned long:          SR_DATATYPE_ULNG, \
        long long:              SR_DATATYPE_LLNG, \
        unsigned long long:     SR_DATATYPE_ULLNG, \
        char:                   SR_DATATYPE_CHR, \
        unsigned char:          SR_DATATYPE_UCHR, \
        char *:                 SR_DATATYPE_STR, \
        unsigned char *:        SR_DATATYPE_USTR, \
        const char *:           SR_DATATYPE_STR, \
        const unsigned char *:  SR_DATATYPE_USTR, \
        float:                  SR_DATATYPE_FLT, \
        double:                 SR_DATATYPE_DBL, \
        struct sr *:            SR_DATATYPE_SR \
), _Generic((A), struct sr *: sr_string_val(0, (A)), default: (A))

#define  S0(A)                                               0
#define  S1(_1)                                              S(_1),0
#define  S2(_1,_2)                                           S(_1),S(_2),0
#define  S3(_1,_2,_3)                                        S(_1),S(_2),S(_3),0
#define  S4(_1,_2,_3,_4)                                     S(_1),S(_2),S(_3),\
    S(_4),0
#define  S5(_1,_2,_3,_4,_5)                                  S(_1),S(_2),S(_3),\
    S(_4),S(_5),0
#define  S6(_1,_2,_3,_4,_5,_6)                               S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),0
#define  S7(_1,_2,_3,_4,_5,_6,_7)                            S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),0
#define  S8(_1,_2,_3,_4,_5,_6,_7,_8)                         S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),0
#define  S9(_1,_2,_3,_4,_5,_6,_7,_8,_9)                      S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),0
#define S10(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A)                   S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),0
#define S11(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B)                S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),S(_B),0
#define S12(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C)             S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),S(_B),S(_C),0
#define S13(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D)          S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),S(_B),S(_C),S(_D),0
#define S14(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E)       S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),S(_B),S(_C),S(_D),S(_E),0
#define S15(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E,_F)    S(_1),S(_2),S(_3),\
    S(_4),S(_5),S(_6),S(_7),S(_8),S(_9),S(_A),S(_B),S(_C),S(_D),S(_E),S(_F),0

#define SR_VAR_(A) S ## A
#define SR_VAR(A, ...) SR_VAR_(A)(__VA_ARGS__)

#define STR_NATIVE  1
#define STR_SR      2

#define S_TYPE(A) _Generic((A), \
        char *:                 STR_NATIVE, \
        unsigned char *:        STR_NATIVE, \
        const char *:           STR_NATIVE, \
        const unsigned char *:  STR_NATIVE, \
        struct sr *:            STR_SR), \
    (A)

struct sr *sr_copy(struct sr *target, int type, const void* source, size_t max);
struct sr *sr_cat(struct sr *target, int type, const void* source, size_t max);
struct sr *sr_concat(struct sr *target, ...);

static inline int sr_string_len(int type, const void *str) {
    return STR_NATIVE == type ? strlen((char *)str) : ((struct sr *)str)->len;
}
/* I have to declare sr_new also here, in order to call it from sr_strdup */
struct sr *sr_new(const char *initval, size_t initsize);
static inline struct sr* sr_strdup(int type, const void *str) {
    return sr_new(STR_NATIVE == type ? (char *)str : ((struct sr *)str)->val, 0);
}
#define F_ANY_ANY(A, B) static inline B sr_ ## A \
                            (int t1, const void *a, int t2, const void* b) { \
    return A(STR_NATIVE == t1 ? (char *)a : ((struct sr *)a)->val, \
             STR_NATIVE == t2 ? (char *)b : ((struct sr *)b)->val); }
#define F_ANY_INT(A, B) static inline B sr_ ## A \
                                        (int t1, const void *a, int b) { \
    return A(STR_NATIVE == t1 ? (char *)a : ((struct sr *)a)->val, b); }

F_ANY_ANY(strstr, char*)
F_ANY_ANY(strcmp, int)
F_ANY_INT(strchr, char*)
F_ANY_INT(strrchr, char*)

#define _CSTR(A) (A)->val
#define _SETLEN(A,B) (A)->len = (B)
#define _STRCLEAR(A) do { \
        (A)->val[0] = 0; \
        (A)->len = 0; \
    } while (0)
#define _STRISEMPTY(A)  !((A)->len)
#define _SPRINTF(A, ...) do { \
        size_t bufsz = snprintf(NULL, 0, __VA_ARGS__); \
        sr_ensure_size(A, bufsz+1, 0); \
        (A)->len = sprintf((A)->val, __VA_ARGS__); \
    } while (0)
#define _RTRIM(A) do { \
        while (isspace((A)->val[(A)->len-1])) ((A)->len)--; \
        (A)->val[(A)->len] = 0; \
    } while (0)
#define _STRFREE(A)  do {\
        if (A) { \
            if ((A)->val) \
                free((A)->val);   \
            free(A); \
            A = NULL; \
        } \
    } while (0)

#endif

