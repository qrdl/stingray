## Header
Include [stringray.h](stingray.h) into your source, and link with Stingray
object file `stingray.o`

## Type
Stingray uses `sr_string` opaque type, application code should not access its
members directly, however there are situations when it is needed, use `CSTR()`
and `SETLEN()` macros.

## Replacement for standard library API
```c
sr_string STRCPY(sr_string, sr_string | char *);
sr_string STRNCPY(sr_string, sr_string | char *, int);
sr_string STRCAT(sr_string, sr_string | char *);
sr_string STRNCAT(sr_string, sr_string | char *);
int STRLEN(sr_string | char *);
char *STRSTR(sr_string | char *, sr_string | char *);
char *STRCMP(sr_string | char *, sr_string | char *);
char *STRCHR(sr_string | char *, int);
char *STRRCHR(sr_string | char *, int);
sr_string STRDUP(sr_string | char *)
```
Expression `sr_string | char *` means that argument could be either C standard
string or `sr_string`, therefore `STRSTR()`, `STRCMP()`, `STRCHR()` and
`STRRCHR()` can be used used with standard C strings instead of standard C
library functions with small performance penalty (extra one function call +
one `if`).
Non-terminated `char` buffers can be used as source for `STRNCAT()` and
`STRNCPY()`

## Stingray-specific functions
#### Creation of new `sr_string`
`sr_new(size_t initval, size_t initsize)`

#### Growing `sr_string` (if needed)
`sr_ensure_size(sr_string, size_t min_size, size_t extend)`

#### Concatenate multiple values of different types
`sr_string CONCAT(sr_string, ...);`
This function concatenates all parameters to `sr_string`, with respect to
parameter types. It returns destination string.
Example:
```c
sr_string foo = sr_new("", 64);
sr_string bar = sr_new("bar", 0);
char *baz = "baz";
CONCAT(foo, "foo", bar, baz, 42, bar[0], (char)' ', 3.141593);
```
will result in `foo` holding the value "foobarbaz42b 3.141593".
**NB!** GCC converts `char` literals to `int`s, therefore in order to be added
as chars, they need to be explicitly casted to `char`. Cast is not required for
`char` variables.

#### Resetting the string
`STRCLEAR(sr_string);`
Sets the string empty

#### Testing the string
`int STRISEMPTY(sr_string)`
Returns 1, if string is empty, 0 otherwise

#### Printing into string
`SPRINTF(sr_string, char *format, ...)`
Prints into string, using `printf` format.

#### Trimming the string
`RTRIM(sr_string)`
Right-trimming the string - removes all space (as defined for `isspace()`) characters

#### Freeing the string
`STRFREE(sr_string)`
Using standard `free()` on Stingray string will not release all memory, therefore use
`STRFREE()`. Calling `STRFREE()` is a safe no-op.

## Accessing the string
String fields should not be accessed directly.

`CSTR(sr_string)`
Returns standard C string of the Stingray string. It is not a copy, it is actual string
data, therefore any changes to this C string will change parent Stingray string!

`SETLEN(sr_string, int)`
Sets the length of the Stingray string. Please note that in order to keep the string
consistent, zero-terminator's position must match the length ot the embedded C string!
And in case of growing the string make sure there is enough space in the string, by
using `sr_ensure_size()`.

