# Stingray
Stringray is a managed string library, that can be used instead of standard
C 0-terminated strings.

## Features
- strings grows automatically, as needed, eleminating the need for
manual resizing
- no risk of buffer overruns
- Stingray strings are fast, and in some situations even faster than standard
library because there is no need to use inefficient `strlen()` and therefore
avoid [Shlemiel The Painter's algorithm](http://www.joelonsoftware.com/articles/fog0000000319.html)
- Stingray contains its own versions of mostly used C standard library
string-related functions that support both standard strings and Stingray
strings, see [API description](API.md)
- in addition to standard functions, Stingray offers number of extra functions
to simplify string handling, see [API description](API.md)

## Dependencies
Stingray doesn't have any dependencies

## Compiling
To compile Stingray, run `make`.
Stingray is written in standard C11 with some GCC extentions, also supported
by Clang, therefore can be compiled with either compiler.
To link an application with Stingray, link your binary with `stingray.o`
See [make](Makefile) for recommended compilation and linking commands.

## Use
In order to use Stingray you need to include [stingray.h](stingray.h) into your
source.  Only `sr_new()` and `sr_ensure_size()` functions can be called
directly, for all other functions use `SR_XXX` macros.
For details see [API description](API.md).
Example can be found in [test](test.c)

## Threads
Stingray is thread-safe (with the standard exception of `STRTOK()`)

## Limitations
- not all standard library functions are replicated
- Stingray doesn't have any special support for wide characters, therefore
`STRLEN()` gives length in bytes, not in characters, and `STRSTR()`, `STRCHR()`
and `STRRCHR()` may match wide characters incorrectly (like match half
character)
- Stingray uses C11's Generics, therefore it cannot be used in projects that
require following pre-C11 standards (like C99).

## License
This project is licensed under the MIT License - see the [license](LICENSE) for
details.

## Author(s)
- Ilya Caramishev - https://gitlab.com/qrdl

