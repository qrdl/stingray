##########################################################################
#
#  File:       Makefile
#
#  Project:    Stingray (https://gitlab.com/qrdl/stingray)
#
#  Descr:      Managed strings library
#
#  Comments:   Makefile for Stingray
#
##########################################################################
#
#  The MIT License (MIT)
#
#  Copyright (c) 2017 Ilya Caramishev
#
#  Permission is hereby granted, free of charge, to any person obtaining a
#  copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,
#  and/or sell copies of the Software, and to permit persons to whom the
#  Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#  DEALINGS IN THE SOFTWARE.
#
##########################################################################/
CFLAGS := -Wall -Wextra -g3 -DDEBUG

.PHONY : all clean stingray

all: stingray

stingray: stingray.o test

stingray.o: stingray.c stingray.h sr_internal.h Makefile
	$(CC) $(CFLAGS) -c -o $@ stingray.c

test: test.c stingray.o
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f test stingray.o

