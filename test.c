#include <stdio.h>
#include "stingray.h"


int main(void) {
    sr_string str = sr_new("", 128);
    sr_string str1 = sr_new("foo", 128);
    STRCPY(str, str1);
    printf("%s (%d)\n", CSTR(str), STRLEN(str));
    STRNCAT(str, "bar", 2);
    printf("%s (%d)\n", CSTR(str), STRLEN(str));
    STRNCAT(str, str1, 10);
    printf("%s (%d)\n", CSTR(str), STRLEN(str));
    CONCAT(str, "text", 5, (char)'-', 100, str1, CSTR(str)[0], 3.141593);
    printf("%s (%d)\n", CSTR(str), STRLEN(str));

    printf("%s\n", STRSTR(str, "text"));
    printf("%s\n", STRSTR(str, str1));
    printf("%s\n", STRSTR("affoooo", str1));

    printf("%s vs %s = %d\n", CSTR(str1), "foo", STRCMP(str1, "foo"));
    printf("%s vs %s = %d\n", CSTR(str1), "z", STRCMP(str1, "z"));
    printf("%s vs %s = %d\n", CSTR(str), CSTR(str1), STRCMP(str, str1));

    printf("%s\n", STRCHR(str, 't'));
    printf("%s\n", STRCHR(str1, 'o'));
    printf("%s\n", STRRCHR("baar", 'a'));

    STRFREE(str);
    STRFREE(str1);

    return 0;
}

