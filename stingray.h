/**************************************************************************
 *
 *  File:       stingray.h
 *
 *  Project:    Stingray (https://gitlab.com/qrdl/stingray)
 *
 *  Descr:      Managed strings library
 *
 *  Comments:   Public API and inlined functions
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef STINGRAY_H
#define STINGRAY_H

#include "sr_internal.h"

typedef struct sr *sr_string;

/* Standard C string API replacement functions */
#define STRCPY(A,B) sr_copy((A), S_TYPE(B), SIZE_MAX)
#define STRNCPY(A,B,N) sr_copy((A), S_TYPE(B), (N))
#define STRCAT(A,B) sr_cat((A), S_TYPE(B), SIZE_MAX)
#define STRNCAT(A,B,N) sr_cat((A), S_TYPE(B), (N))
#define STRLEN(A) sr_string_len(S_TYPE(A))
#define STRSTR(A,B) sr_strstr(S_TYPE(A), S_TYPE(B))
#define STRCMP(A,B) sr_strcmp(S_TYPE(A), S_TYPE(B))
#define STRCHR(A,B) sr_strchr(S_TYPE(A), (B))
#define STRRCHR(A,B) sr_strrchr(S_TYPE(A), (B))
#define STRDUP(A) sr_strdup(S_TYPE(A))

/* Stingray additional functions */
#define CONCAT(A, ...) sr_concat((A), \
        SR_VAR(NUMARGS(__VA_ARGS__), ##__VA_ARGS__))
#define CSTR(A) _CSTR(A)
#define STRCLEAR(A) _STRCLEAR(A)
#define STRISEMPTY(A)  _STRISEMPTY(A)
#define SPRINTF(A, ...) _SPRINTF(A, __VA_ARGC__)
#define RTRIM(A) _RTRIM(A)
#define STRFREE(A) _STRFREE(A)
#define SETLEN(A,B) _SETLEN((A),(B))

/* TODO
STRINS
STRREPLACE
STRSUBS
STRREAD
*/

sr_string sr_new(const char *initval, size_t initsize);
void sr_ensure_size(sr_string str, size_t minsize, size_t extend);

#endif

